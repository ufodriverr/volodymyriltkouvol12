﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class src_Tower : MonoBehaviour {

	int BulletShooted = 0;
	float Timer = 0.0f;
	float Timer2 = 0.0f;
	public GameObject Bullet;
	public int WaitSec = 0;
	bool Active = false;
	Text TowersTxt;
	bool Stage1 = true;
	bool Stage3 = false;

	// Use this for initialization
	void Start () {
		TowersTxt = GameObject.Find ("Numver").GetComponent<Text> ();
		TowersTxt.text = "" + (int.Parse (TowersTxt.text) + 1);
	}
	
	// Update is called once per frame
	void Update () {
		Stage1 = GameObject.Find ("Camera").GetComponent<scr_GameControl> ().CreateNew;
		if (Stage1) {
			Timer2 += Time.deltaTime;
			if (Active) {
				Timer += Time.deltaTime;
				if (Timer > 0.5) {
					Timer = 0;
					TurnAndShoot ();
					if(BulletShooted==12){
						Active=false;
					};
				}
			} else {
				GetComponent<SpriteRenderer> ().color = Color.white;
			}
			;
			if (Timer2 >= WaitSec) {
				GetComponent<SpriteRenderer> ().color = Color.red;
				Active = true;
			}
			;
		} else if(!Stage3){
			Stage3 = true;
			GetComponent<SpriteRenderer> ().color = Color.red;
			Active = true;
			BulletShooted=0;
			Timer = 0;
		}else{
			if(Active){
				Timer += Time.deltaTime;
				if (Timer > 0.5) {
					Timer = 0;
					TurnAndShoot ();
					if(BulletShooted==12){
						Active=false;
					};
				};
			}else{
				GetComponent<SpriteRenderer> ().color = Color.white;
			};
		}
	}

	void TurnAndShoot(){
		BulletShooted += 1;
		transform.rotation = Quaternion.Euler (0, 0, transform.rotation.eulerAngles.z + Random.Range (15, 45));
		Quaternion Rot = transform.rotation;
		Instantiate (Bullet, transform.position, Rot);
	}

	void OnDestroy(){
		TowersTxt.text = "" + (int.Parse (TowersTxt.text) - 1);
	}
}
