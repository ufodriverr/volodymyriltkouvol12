﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class scr_Bullet : MonoBehaviour {

	public GameObject Tower;
	public float speed = 4.0f;
	float Traveled = 0;
	float NeedDist = 0;
	Vector3 StartPos;
	bool CreateNew = true;
	// Use this for initialization
	void Start () {
		StartPos = transform.position;
		NeedDist = Random.Range (1.0f, 4.0f);
	}
	
	// Update is called once per frame
	void Update () {
		CreateNew = GameObject.Find ("Camera").GetComponent<scr_GameControl> ().CreateNew;
		Traveled = Vector3.Distance(transform.position, StartPos);
		transform.position += transform.rotation* new Vector3 (0, speed * Time.deltaTime,0);
		if (Traveled >= NeedDist) {
			Destroy(gameObject);
			if(CreateNew){
				(Instantiate (Tower, transform.position, Quaternion.identity)as GameObject).GetComponent<src_Tower>().WaitSec = 6;
			};
		};
	}

	void OnCollisionEnter2D(Collision2D coll) {
		if (coll.gameObject.tag == "Tower") {
			if (Traveled>0.5f) {
				Destroy(coll.gameObject);
				Destroy(gameObject);
			};
		};
	}
}
